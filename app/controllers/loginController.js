angular.module('loginApp', [])

.controller('loginController', ['Flash', 'getUserData', 'commonProp', '$scope', '$firebaseAuth', '$log', '$location', function(Flash, getUserData, commonProp, $scope, $firebaseAuth, $log, $location) {
    var tmpUser = {}
    console.log("home controller has been loaded")
    if(commonProp.getUser()){
    commonProp.logoutUser()

    }
    $scope.userlevel = commonProp.getUserLevel()
    console.log("USER level is" + $scope.userlevel)
    // if ($scope.userlevel == 1) {

    //     $location.path('/admin')
    // }
    $scope.signIn = function() {
        var username = $scope.user.email
        var pass = $scope.user.pass
        $('#btnlogin').hide()
        $('#btnlogin2').show()

        //Authentication of the user
        var fire = new Firebase("https://kuzakuza.firebaseio.com/")
        var login = $firebaseAuth(fire)

        login.$authWithPassword({ email: username, password: pass })
            .then(function(res) {
                // laddaL.login.loading  = false
                $log.info("Authentication Successful" + username)
                commonProp.setUser(username)
                commonProp.setUserLevel("0")

                if (commonProp.getUser()) {
                    // $scope.user
                    $scope.usersCourses = getUserData

                }

                $('#btnlogin2').hide()
                $('#btnlogin').show()
                $location.path('/mydashboard')
            }, function(error) {
                $scope.regError = true
                    // ladda.login.loading  = false
                $scope.regErrorMessage = error.message;
                $log.info("Failed to Authenticate")
                $('#btnlogin2').hide()
                $('#btnlogin').show()
                var message = '<strong>Invalid username or password</strong>'
                var id = Flash.create('danger', message, 3000, { class: 'custom-class', id: 'custom-id' }, true)

            })



    }


}])
