// angular.module('kuzaApp', ['dashboardApp','ngFlash','ngRoute','dashApp','firebase','ngMessages'])
angular.module('kuzaApp', ['homeApp','ngRoute','ngFlash','dataF','firebase','ngMessages','dashboardApp'])

.config(['$routeProvider', function($routeProvider

  ) {

  $routeProvider
    .when('/',{
    templateUrl: 'app/landing.html',
    controller: 'homeController'

  })
    .when('/dashboard',{
    templateUrl: 'pages/partials/dashboard.html',
    controller: 'dashboardController'
  })
        .when('/kits',{
    templateUrl: 'pages/kits.html',
    controller: 'dashboardController'
  })
  .when('/addKit',{
    templateUrl: 'pages/partials/add_kit.html',
    controller: 'dashboardController'
  })
  .when('/farm-visits',{
    templateUrl: 'pages/farm-visits.html',
    controller: 'dashboardController'
  })
      .when('/login',{
    templateUrl: 'pages/login.html',
    controller: 'homeController'
  })
            .when('/profile',{
    templateUrl: 'pages/profile.html',
    controller: 'homeController'
  })
      .otherwise(
    {redirectTo: '/notfound'});
}])

// .filter('startFrom', function() {
//     return function(input, start) {
//         start = +start; //parse to int
//         return input.slice(start);
//     }
// })

