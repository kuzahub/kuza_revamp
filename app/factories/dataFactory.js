angular.module('dataF', [])

.factory('dataFactory', ['$firebaseObject', '$parse', '$rootScope', 'Flash', '$q', '$firebaseArray', '$location', '$firebaseAuth',
    function($firebaseObject, $parse, $rootScope, Flash, $q, $firebaseArray, $location, $firebaseAuth) {
        var reg = new Firebase("https://simplex0.firebaseio.com/regusers")
        var u = $firebaseArray(reg)
        var user = ''
        var userLevel = ''
            // var log = new Firebase('https://simplex0.firebaseio.com')

        return {

            saveUser: function(id, username, fname, lname, pnumber) {
                console.log(id + "||" + username + "||" + fname + "||" + lname + "||" + pnumber)
                var reg = new Firebase("https://simplex0.firebaseio.com/regusers/" + id)
                var u = $firebaseArray(reg)

                u.$add({
                        loginID: id,
                        email: username,
                        online: 'false',
                        coursesSubscribed: '0',
                        first_name: fname,
                        last_name: lname,
                        pnumber: pnumber
                    })
                    .then(function(res) {
                        console.log(res + "posted successfully")

                    }, function(err) {
                        console.log(err)
                    })

            },
            bookVisit: function() {
                var message = '<strong>Success!</strong> We have been notified of your farm visit dates. We will get back to you shortly.'
                var id = Flash.create('success', message, 3000, { class: 'custom-class', id: 'custom-id' }, true)


            },
            saveDataToUser: function(user_id_, courseID) {
                console.log(user_id_ + "the course ID  is   " + courseID)
                var save = new Firebase("https://simplex0.firebaseio.com/regusers/")
                var u = $firebaseArray(save)

                u.$loaded().then(function(t) {
                    var def = $q.defer()
                    var uv = t.$getRecord(user_id_)
                    if (uv != null) {
                        def.resolve(uv)
                            // console.log(uv)
                        var ma = /"[\-)]\w+/g
                        var user_id = ma.exec(JSON.stringify(uv))
                        user_id = user_id.toString().replace(/['"]+/g, '')
                        console.log(user_id.charAt(0) + "first character!")
                        if (user_id.charAt(0) != "-") {
                            user_id = "-" + user_id
                            console.log(user_id)
                        }

                        console.log(user_id)
                        var getuser = new Firebase("https://simplex0.firebaseio.com/regusers/" + user_id_ + "/" + user_id)
                        var data = $firebaseArray(getuser)
                            // $rootScope.userDarta = f2
                        def.resolve(data)
                        console.log(data + "oldoldoldold")
                        $rootScope.userAssignData = data

                        var saveu = new Firebase("https://simplex0.firebaseio.com/regusers/" + user_id_ + "/" + user_id + "/coursesSubscribed/" + courseID)
                        var userdata = $firebaseArray(saveu)


                        var def = $q.defer()
                        userdata.$loaded().then(function(data) {
                            def.resolve(data)
                            console.log(data + " ||| " + data.length)
                            if (data == '') {
                                console.log("nothing - no courses")
                                userdata.$add({
                                    coursesSubscribed: courseID
                                        // emailId: $scope.projectToUpdate.userID
                                }).then(function(res) {
                                    // res.key == 
                                    console.log("success")
                                        // $('#editModal').modal('hide')
                                    $('#editModal').modal('hide')
                                    var message = '<strong>Success!</strong> The course has been assigned to the user.'
                                    var id = Flash.create('success', message, 3000, { class: 'custom-class', id: 'custom-id' }, true)
                                }, function(err) {
                                    console.log(err)
                                })
                            } else {
                                for (var i = 0; i < data.length; i++) {
                                    console.log("***the course exists for this user : " + courseID + " === " + userdata[i].coursesSubscribed)
                                        // $scope.msg = "This course already assigned to user"
                                    $('#editModal').modal('hide')
                                    var message = '<strong>Not saved!</strong> The course is currently assigned to this user.'
                                    var id = Flash.create('danger', message, 3000, { class: 'custom-class', id: 'custom-id' }, true)

                                }
                            }

                            console.log(JSON.stringify(data) + " | haha damn users")
                                // $rootScope.availableCourses = x
                        })

                    }
                })

            },

            getUserLevel: function() {
                if (userLevel == '') {
                    userLevel = localStorage.getItem('userLevel')
                }
                return userLevel
            },
            setUserLevel: function(value) {
                userLevel = value
                localStorage.setItem("userLevel", value)
                console.log("User value: " + value)


            },
            setUsersOnline: function(id) {
                var theID = reg.getRecord(id)
                theID.online = 'true'
                reg.$save()

            },
            deleteThatCourse: function(id) {
                console.log("dededed" + id)
                var remi = new Firebase("https://simplex0.firebaseio.com/courses/coursedemo/" + id)
                var itemtodelete = $firebaseObject(remi)
                itemtodelete.$remove(id).then(function(res) {
                        // data has been deleted locally and in the database
                        $('#deleteModal').modal('hide')
                        $location.path('/admindashboard')
                        var message = '<center><strong>The course has been deleted.</strong> </center>'
                        var id = Flash.create('danger', message, 3000, { class: 'custom-class', id: 'custom-id' }, true)

                        console.log("reeeemmmoooveeedd!!")
                    }, function(error) {
                        console.log("Error:", error)
                            // return $firebaseArray(fire)
                    })
                    // console.log("loaded" + $scope.data)


            },
            getUser: function() {
                if (user == '') {
                    user = localStorage.getItem('userEmail')
                }
                return user
            },
            getAllUsers: function() {
                var reg = new Firebase("https://simplex0.firebaseio.com/regusers")
                var x = $firebaseArray(reg)
                    // var keys = x.$getIndex()
                var def = $q.defer()

                x.$loaded().then(function(data) {
                    def.resolve(x)
                    console.log(x.length + "haha")
                    $rootScope.usersL = x

                })
                return x
            },
            getAllCourses: function() {
                var courses = new Firebase("https://simplex0.firebaseio.com/courses/coursedemo")
                var x = $firebaseArray(courses)
                    // var keys = x.$getIndex()
                var def = $q.defer()

                x.$loaded().then(function(data) {
                    def.resolve(x)
                    console.log(x + "haha damn courses")
                    $rootScope.availableCourses = x

                })
                console.log(x + "haha")
                return x
            },



            getUserData: function() {
                // var j = {}
                // var def = $q.defer()
                // console.log(id)

                firebase.User.$loaded().then(function(t) {
                    console.log("loading... " + t)
                    
     

                }).catch(function(error) {
                    console.log("Error:", error)
                })
                return t
                    // return "olright"
            },
            setUser: function(value) {
                user = value
                localStorage.setItem("userEmail", value)
                console.log("User set: " + value)
            },
            logoutUser: function() {
                // logout the user
                // logObj.logout()
                console.log("logged out")
                localStorage.removeItem('userEmail')
                localStorage.removeItem('userLevel')

                $location.path('/landing')
            }
        }
    }
])

.factory('Auth', function($firebaseAuth) {
    var fire = new Firebase("https://simplex0.firebaseio.com/registeredgitUsers")
    return $firebaseAuth(fire)

})

.factory('getProjects', function($firebaseArray) {
        var fire = new Firebase("https://simplex0.firebaseio.com/courses/coursedemo")
        return $firebaseArray(fire)
        console.log("loaded" + $scope.data)
    })
    .factory('getThatCourse', function($firebaseArray, id) {
        var fire = new Firebase("https://simplex0.firebaseio.com/courses/coursedemo/courseepisodes/" + id)
        return $firebaseArray(fire)
        console.log("loaded" + $scope.data)
    })
    .factory('getUserData', function($firebaseArray) {
                // var def = $q.defer()
                // console.log(id)

                var x = firebase.User.email
                console.log("its "+x)
    })

.factory('getAllUsers', function($firebaseArray, $firebaseObject) {
    var usersRef = new Firebase('https://simplex0.firebaseio.com/users');
    var users = $firebaseArray(usersRef)
    var Users = {
        getProfile: function(uid) {
            return $firebaseObject(usersRef.child(uid));
        },
        getDisplayName: function(uid) {
            return users.$getRecord(uid).displayName;
        },
        all: users
    }

    console.log(users)
    return Users
})
