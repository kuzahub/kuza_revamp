# install dependencies
npm install

# serve with hot reload at localhost:8000
node server/app

# run all tests
npm test